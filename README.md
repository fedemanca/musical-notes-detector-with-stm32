# Musical notes detector with STM32



## Description

This project was done for the final exam of the course "Sistemi a Microcontrollore". It uses a STM32 microcontroller to detect the frequency of some musical notes, by using UART and Timers. The STM HAL (Hardware Abstraction Layer) is used in this simple project.
